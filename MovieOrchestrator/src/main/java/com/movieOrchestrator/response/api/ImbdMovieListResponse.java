package com.movieOrchestrator.response.api;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImbdMovieListResponse {

	private ArrayList<ImdbMovieDetails> Search;

	@JsonProperty("Search")
	public ArrayList<ImdbMovieDetails> getSearch() {
		return Search;
	}

	public void setSearch(ArrayList<ImdbMovieDetails> search) {
		Search = search;
	}
	
	
	
}
