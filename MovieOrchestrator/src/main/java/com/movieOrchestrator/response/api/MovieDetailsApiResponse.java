package com.movieOrchestrator.response.api;

public class MovieDetailsApiResponse {
	
	private String movieName;
	private String movieReleaseYear;
	
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieReleaseYear() {
		return movieReleaseYear;
	}
	public void setMovieReleaseYear(String movieReleaseYear) {
		this.movieReleaseYear = movieReleaseYear;
	}
	
	
	

}
