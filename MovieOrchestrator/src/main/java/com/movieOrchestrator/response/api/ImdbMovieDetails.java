package com.movieOrchestrator.response.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImdbMovieDetails {
	
	private String  Poster;
	private String Year;
	
	
	@JsonProperty("Year")
	public String getYear() {
		return Year;
	}

	public void setYear(String year) {
		Year = year;
	}

	@JsonProperty("Poster")
	public String getPoster() {
		return Poster;
	}

	public void setPoster(String poster) {
		Poster = poster;
	}
	
	

}
