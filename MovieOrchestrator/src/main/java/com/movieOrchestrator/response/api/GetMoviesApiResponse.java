package com.movieOrchestrator.response.api;

import java.util.ArrayList;

public class GetMoviesApiResponse {

	private String status;
	private String statusText;
	private ArrayList<MovieDetailsApiResponse> movieDetails;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusText() {
		return statusText;
	}
	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}
	public ArrayList<MovieDetailsApiResponse> getMovieDetails() {
		return movieDetails;
	}
	public void setMovieDetails(ArrayList<MovieDetailsApiResponse> movieDetails) {
		this.movieDetails = movieDetails;
	}
	
	
}
