package com.movieOrchestrator.response;

import java.util.ArrayList;

public class GetMoviesResponse {

	private String status;
	private ArrayList<MovieDetails> movieDetails;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<MovieDetails> getMovieDetails() {
		return movieDetails;
	}
	public void setMovieDetails(ArrayList<MovieDetails> movieDetails) {
		this.movieDetails = movieDetails;
	}
	
	
}
