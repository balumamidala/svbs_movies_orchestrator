package com.movieOrchestrator.service;

import java.net.URI;
import java.util.ArrayList;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.movieOrchestrator.response.GetMovieIdResponse;
import com.movieOrchestrator.response.GetMoviesResponse;
import com.movieOrchestrator.response.MovieDetails;
import com.movieOrchestrator.response.api.GetMovieIdApiResponse;
import com.movieOrchestrator.response.api.GetMoviesApiResponse;
import com.movieOrchestrator.response.api.ImbdMovieListResponse;

public class MovieService {

	public GetMoviesResponse getMovieDetails() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<GetMoviesApiResponse> getMoviesApiResponseEntity = restTemplate
				.getForEntity("http://localhost:8080/movies", GetMoviesApiResponse.class);
		GetMoviesApiResponse getMoviesApiResponse = getMoviesApiResponseEntity.getBody();

		GetMoviesResponse getMoviesResponse = new GetMoviesResponse();
		ArrayList<MovieDetails> movieDetailsList = new ArrayList<>();
		if (getMoviesApiResponse.getStatus().equals("success")) {

			for (int i = 0; i < getMoviesApiResponse.getMovieDetails().size(); i++) {
				MovieDetails movieDetails = new MovieDetails();
				movieDetails.setMovieTitle(getMoviesApiResponse.getMovieDetails().get(i).getMovieName());
				movieDetails.setReleaseYear(getMoviesApiResponse.getMovieDetails().get(i).getMovieReleaseYear());
				movieDetailsList.add(movieDetails);

			}

			getMoviesResponse.setMovieDetails(movieDetailsList);

		}
		return getMoviesResponse;
	}

	public GetMoviesResponse getMoviePoster(GetMoviesResponse getMoviesResponse) {
		RestTemplate restTemplate = new RestTemplate();
		try {
			for (MovieDetails movieDetails : getMoviesResponse.getMovieDetails()) {
				String url = "https://movie-database-imdb-alternative.p.rapidapi.com?s=" + movieDetails.getMovieTitle();
				URI uri = new URI(url);
				HttpHeaders headers = new HttpHeaders();
				headers.set("x-rapidapi-host", "movie-database-imdb-alternative.p.rapidapi.com");
				headers.set("x-rapidapi-key", "twgGUyGffAmshkNvS6yX73PYTO8dp1fHdzvjsnehKTZMcM1PzZ");
				HttpEntity requestEntity = new HttpEntity<>(headers);
				ResponseEntity<ImbdMovieListResponse> imbdMovieListEntity = restTemplate.exchange(uri, HttpMethod.GET,
						requestEntity, ImbdMovieListResponse.class);
				ImbdMovieListResponse imbdMovieListResponse = imbdMovieListEntity.getBody();
				for (int i = 0; i < imbdMovieListResponse.getSearch().size(); i++) {
					if (movieDetails.getReleaseYear().equals(imbdMovieListResponse.getSearch().get(i).getYear())) {
						movieDetails.setPoster(imbdMovieListResponse.getSearch().get(i).getPoster());
						break;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return getMoviesResponse;

	}

	public GetMovieIdResponse getMovieDetails(String id) {
		RestTemplate restTemplate = new RestTemplate();
		GetMovieIdResponse getMovieIdResponse = new GetMovieIdResponse();
		try {

			String url = "https://movie-database-imdb-alternative.p.rapidapi.com?i=" + id;
			URI uri = new URI(url);
			HttpHeaders headers = new HttpHeaders();
			headers.set("x-rapidapi-host", "movie-database-imdb-alternative.p.rapidapi.com");
			headers.set("x-rapidapi-key", "twgGUyGffAmshkNvS6yX73PYTO8dp1fHdzvjsnehKTZMcM1PzZ");
			HttpEntity entity = new HttpEntity<>(headers);
			ResponseEntity<GetMovieIdApiResponse> getMovieIdEntity = restTemplate.exchange(uri, HttpMethod.GET, entity,
					GetMovieIdApiResponse.class);
			GetMovieIdApiResponse getMovieIdApiResponse = getMovieIdEntity.getBody();
			String title  =getMovieIdApiResponse.getTitle();
			String actors = getMovieIdApiResponse.getActors();
			String director =getMovieIdApiResponse.getDirector();
			String genre =getMovieIdApiResponse.getGenre();
			String imdbId =getMovieIdApiResponse.getImdbID();
			String imdbRating =getMovieIdApiResponse.getImdbRating();
			String imdbVotes =getMovieIdApiResponse.getImdbVotes();
			String language =getMovieIdApiResponse.getLanguage();
			String poster =getMovieIdApiResponse.getPoster();
			String rated =getMovieIdApiResponse.getRated();
			String year =getMovieIdApiResponse.getYear();
			String writer =getMovieIdApiResponse.getWriter();
			String runTime =getMovieIdApiResponse.getRuntime();
			String released =getMovieIdApiResponse.getReleased();
			
			
			getMovieIdResponse.setTitle(title);
			getMovieIdResponse.setDirector(director);
			getMovieIdResponse.setActors(actors);
			getMovieIdResponse.setGenre(genre);
			getMovieIdResponse.setImdbID(imdbId);
			getMovieIdResponse.setImdbRating(imdbRating);
			getMovieIdResponse.setImdbVotes(imdbVotes);
			getMovieIdResponse.setLanguage(language);
			getMovieIdResponse.setPoster(poster);
			getMovieIdResponse.setRated(rated);
			getMovieIdResponse.setReleased(released);
			getMovieIdResponse.setRuntime(runTime);
			getMovieIdResponse.setWriter(writer);
			getMovieIdResponse.setYear(year);
			
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return getMovieIdResponse;
		
	}

}
