package com.movieOrchestrator.movieController;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.movieOrchestrator.response.GetMovieIdResponse;
import com.movieOrchestrator.response.GetMoviesResponse;
import com.movieOrchestrator.service.MovieService;

@RestController
public class MovieController {
	@GetMapping("/movies")
	public GetMoviesResponse getMovies() {
		MovieService movieService = new MovieService();
		GetMoviesResponse getMovieResponse = movieService.getMovieDetails();
		 getMovieResponse = movieService.getMoviePoster(getMovieResponse);
		
		return getMovieResponse;
	}

	@GetMapping("/movie/{id}")
	public GetMovieIdResponse getMovieDetails(@PathVariable("id") String id) {
		MovieService movieService = new MovieService();
		GetMovieIdResponse movieIdResponse = movieService.getMovieDetails(id);
		
		if(movieIdResponse != null) {
			movieIdResponse.setStatus("success");
			
		}
		return movieIdResponse;
		
	}
}
